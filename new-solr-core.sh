#!/usr/bin/env bash

export NEW_SOLR_CORE=${NEW_SOLR_CORE}

if [ ! -e "/opt/solr/server/solr/${NEW_SOLR_CORE}" ]; then

    solr create_core -c ${NEW_SOLR_CORE} && \
    rm -R /opt/solr/server/solr/${NEW_SOLR_CORE}/conf && \
    cp -R /opt/solr/server/solr/configsets/drupal/conf /opt/solr/server/solr/${NEW_SOLR_CORE}/conf

else
  echo "Solr core already created"
fi

exec "$@"

# Don't forget to restart this container afterwards for configuration changes to take effect